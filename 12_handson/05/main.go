/*
Create a data structure to pass to a template which
contains information about restaurant's menu including Breakfast, Lunch, and Dinner items
*/

package main

import (
	"log"
	"os"
	"text/template"
)

type item struct {
	Description string
}

type menu struct {
	Breakfast []item
	Lunch     []item
	Dinner    []item
}

type restaurant struct {
	Name string
	Menu []menu
}

var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseFiles("tpl.tmpl"))
}

func main() {
	restaurants := []restaurant{
		restaurant{
			Name: "First restaurant",
			Menu: []menu{
				menu{
					Breakfast: []item{
						item{"Cacao"},
						item{"Croissant"},
						item{"Yahourt"},
					},
					Lunch: []item{
						item{"Omelette"},
						item{"Sandwich"},
					},
					Dinner: []item{
						item{"Coq au vin"},
						item{"Escalope"},
					},
				},
			},
		},
		restaurant{
			Name: "Second restaurant",
			Menu: []menu{
				menu{
					Breakfast: []item{
						item{"Cacao"},
						item{"Croissant"},
						item{"Yahourt"},
					},
					Lunch: []item{
						item{"Omelette"},
						item{"Sandwich"},
					},
					Dinner: []item{
						item{"Coq au vin"},
						item{"Escalope"},
					},
				},
			},
		},
	}
	err := tpl.Execute(os.Stdout, restaurants)
	if err != nil {
		log.Fatalln(err)
	}
}
