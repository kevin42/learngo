/*
* 1. Create a data structure to pass to a template which
* contains information about California hotels including Name, Address, City, Zip, Region
* region can be: Southern, Central, Northern
* can hold an unlimited number of hotels
 */

package main

import (
	"html/template"
	"log"
	"os"
)

type region struct {
	Southern bool
	Central  bool
	Northern bool
}
type hotel struct {
	Name    string
	Address string
	City    string
	Zip     string
	Region  region
}

func (h *hotel) ShowRegion() string {
	switch {
	case h.Region.Southern:
		return "Southern"
	case h.Region.Central:
		return "Central"
	case h.Region.Northern:
		return "Northern"
	}
	return ""
}

var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseFiles("tpl.gohtml"))
}

func main() {
	hotels := []hotel{
		hotel{
			"First hotel",
			"25 rue du lac",
			"San Francisco",
			"SF2000",
			region{Central: true},
		},
		hotel{
			"Second hotel",
			"25 rue du lac",
			"San Francisco",
			"SF2000",
			region{Northern: true},
		},
	}
	err := tpl.Execute(os.Stdout, hotels)
	if err != nil {
		log.Fatalln(err)
	}
}
