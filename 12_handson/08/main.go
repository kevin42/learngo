/*
Parse this CSV file, putting two fields from the contents of the CSV file into a data structure.
Parse an html template, then pass the data from step 1 into the CSV template;
have the html template display the CSV data in a web page.
*/
package main

import (
	"encoding/csv"
	"io"
	"log"
	"net/http"
	"os"
	"text/template"
)

type dataRow struct {
	Date   string
	Open   string
	High   string
	Low    string
	Close  string
	Volume string
	Adj    string
}

var tpl *template.Template
var data []dataRow
var layout = "2006-01-02"

func init() {
	tpl = template.Must(template.ParseFiles("table.tmpl"))
}
func main() {
	f, err := os.Open("table.csv")
	if err != nil {
		log.Fatalln(err)
	}
	defer f.Close()
	reader := csv.NewReader(f)

	i := 0
	for {
		row, err := reader.Read()
		if err != nil {
			if err == io.EOF {
				break
			}
			log.Fatalln(err)
		}

		if i == 0 {
			i++
			continue
		}

		newRow := dataRow{
			Date:   row[0],
			Open:   row[1],
			High:   row[2],
			Low:    row[3],
			Close:  row[4],
			Volume: row[5],
			Adj:    row[6],
		}
		data = append(data, newRow)
	}

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		err = tpl.Execute(w, data)
		if err != nil {
			log.Fatalln(err)
		}

	})
	log.Fatal(http.ListenAndServe(":8000", nil))
}
