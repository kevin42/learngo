package main

import (
	"log"
	"math"
	"os"
	"text/template"
	"time"
)

var tpl *template.Template

var fm = template.FuncMap{
	"double": double,
	"square": square,
	"date":   date,
}

func double(x int) int {
	return x * 2
}

func square(x int) float64 {
	return math.Pow(float64(x), 2)
}

func date(t time.Time) string {
	return t.Format("02-01-2006")
}

func init() {
	tpl = template.Must(template.New("").Funcs(fm).ParseFiles("tpl.gohtml"))
}

type c struct {
	N int
	T time.Time
}

func main() {
	err := tpl.ExecuteTemplate(os.Stdout, "tpl.gohtml", c{
		3,
		time.Now(),
	})
	if err != nil {
		log.Fatalln("Something went wrong", err)
	}

}
