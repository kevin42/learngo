package main

import "fmt"

type person struct {
	fName string
	lName string
}

type secretAgent struct {
	person
	licencedToKill bool
}

type shootName interface {
	speak()
}

func say(s shootName) {
	s.speak()
}

func (p *person) speak() {
	fmt.Printf("Hello, my name is %s!\n", p.fName)
}

func (sa *secretAgent) speak() {
	fmt.Printf("My name is %s %s, licenced to kill: %t\n", sa.fName, sa.lName, sa.licencedToKill)
}

func main() {
	p := person{"Louison", "Bobet"}
	say(&p)

	sa := secretAgent{person{"James", "Bond"}, true}
	say(&sa)
}
