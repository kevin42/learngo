package main

import (
	"fmt"
	"math"
)

type square struct {
	side float64
}

type circle struct {
	r float64
}

func (s square) area() float64 {
	return s.side * s.side
}

type shape interface {
	area() float64
}

func (c circle) area() float64 {
	return math.Pi * c.r * c.r
}

func info(a shape) {
	fmt.Println(a.area())
}

func main() {
	s := square{10}
	c := circle{5}
	info(s)
	info(c)
}
