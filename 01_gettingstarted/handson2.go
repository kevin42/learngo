package main

import "fmt"

type person struct {
	fName string
	lName string
}

type secretAgent struct {
	person    person
	agentType string
}

func (p person) speak() {
	fmt.Println("Boo")
}

func (sa secretAgent) speak() {
	fmt.Println("Shh")
}

func main() {
	p := person{"Louison", "Bobet"}
	p2 := person{"James", "Bond"}
	sa := secretAgent{p2, "MI5"}

	fmt.Println(p.fName)
	p.speak()

	fmt.Println(sa.person.fName)
	fmt.Println(sa.agentType)
	sa.speak()
	sa.person.speak()
}
