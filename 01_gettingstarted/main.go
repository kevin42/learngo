package main

import "fmt"

var x int

type Person struct {
	fname string
	lname string
}

type secretAgent struct {
	Person
	licenseToKill bool
}

func (p Person) speak() {
	fmt.Println(p.fname, `says, "Good morning!"`)
}

func (sa secretAgent) speak() {
	fmt.Println(sa.fname, sa.licenseToKill, `says, "Good morning!"`)
}

type human interface {
	speak()
}

func saySomething(h human) {
	h.speak()
}

func main() {
	fmt.Printf("%T", x)
	fmt.Println(x)

	// composite literal
	xi := []int{2, 3, 4, 5, 6, 7}
	fmt.Println(xi)

	m := map[string]int{
		"Kevin": 34,
	}
	fmt.Println(m)

	p1 := Person{
		"Miss",
		"Moneypenny",
	}
	fmt.Println(p1)
	p1.speak()

	sal := secretAgent{
		p1,
		true,
	}
	sal.speak()
	sal.Person.speak()

	// polymorphism
	saySomething(sal)
	saySomething(p1)

	// control structure
	for i := 0; i < 10; i++ {
		fmt.Printf("i: %d\n", i)
	}

	for key, value := range []int{1, 2, 3} {
		fmt.Printf("%d: %d\n", key, value)
	}

	for key, value := range m {
		B
		fmt.Printf("%s: %s\n", key, value)
	}
}
