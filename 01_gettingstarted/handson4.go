package main

import "fmt"

type person struct {
	fName   string
	favFood []string
}

type vehicle struct {
	doors  int
	colour string
}

type truck struct {
	vehicle
	fourWheel bool
}

type sedan struct {
	vehicle
	luxury bool
}

type transportation interface {
	transportationDevice() string
}

type gator int

func (g *gator) greeting() {
	fmt.Println("Hello, I am a gator")
}

type flamingo bool

func (f *flamingo) greeting() {
	fmt.Println("Hello, I am pink and beautiful and wonderful")
}

type swampCreature interface {
	greeting()
}

func bayou(sc swampCreature) {
	sc.greeting()
}

func (p *person) walk() string {
	s := fmt.Sprintf("%s is walking", p.fName)
	return s
}

func (t *truck) transportationDevice() string {
	return "something"
}

func (se *sedan) transportationDevice() string {
	return "something else"
}

func report(t transportation) {
	fmt.Println(t.transportationDevice())
}

func main() {
	// slice of int
	s := []int{1, 2, 3, 4, 5}
	fmt.Printf("%#v\n", s)

	for i, _ := range s {
		fmt.Println(i)
	}

	for i, n := range s {
		fmt.Println(n, i)
	}

	// map
	m := make(map[string]int)
	m["kevin"] = 34
	m["gemma"] = 32
	m["ciara"] = 0

	fmt.Printf("%#v\n", m)

	for k := range m {
		fmt.Println(k)
	}

	for k, v := range m {
		fmt.Println(k, v)
	}

	// struct
	p1 := person{
		"kevin",
		[]string{"pasta"},
	}
	fmt.Printf("%#v\n", p1)
	fmt.Println(p1.fName)
	fmt.Println(p1.favFood)

	for i, v := range p1.favFood {
		fmt.Println(i, v)
	}

	fmt.Println(p1.walk())

	t := truck{
		vehicle:   vehicle{doors: 4, colour: "white"},
		fourWheel: true,
	}

	se := sedan{
		vehicle: vehicle{doors: 2, colour: "blue"},
		luxury:  true,
	}
	fmt.Printf("%v\n", t)
	fmt.Printf("%v\n", se)

	fmt.Println(t.doors)
	fmt.Println(se.luxury)

	fmt.Println(t.transportationDevice())
	fmt.Println(se.transportationDevice())

	report(&t)
	report(&se)

	var g1 gator
	g1 = 42
	fmt.Printf("%T\n", g1)

	var x int
	fmt.Println(x)
	fmt.Printf("%T\n", x)

	g1 = gator(x)
	fmt.Println(g1)
	fmt.Printf("%T\n", g1)

	g1.greeting()

	var f flamingo
	f = true
	bayou(&f)

	sorry := "I'm sorry I can't do that"
	fmt.Println(sorry)
	fmt.Println([]byte(sorry))
	fmt.Println(string([]byte(sorry)))
	fmt.Println(string([]byte("I'm sorry dave")))
	fmt.Println(string([]byte("Dave I can't")))
	fmt.Println(string([]byte("Can't do that")))

	for _, c := range []byte(sorry) {
		fmt.Println(string(c))
	}
}
