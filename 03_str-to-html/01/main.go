package main

import "fmt"

func main() {
	name := "Kevin Etienne"
	tpl := `
    <!DOCTYPE html>
    <html>
    <body>
    <h1>Hello ` + name + `!</h1>
    </body>
    </html>
    `
	fmt.Println(tpl)
}
