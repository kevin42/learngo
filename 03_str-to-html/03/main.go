package main

import (
	"fmt"
	"io"
	"log"
	"os"
	"strings"
)

func main() {
	name := os.Args[1]
	str := fmt.Sprint(`
    <h1>` + name + `</h1>
    `)
	f, err := os.Create("index.html")
	if err != nil {
		log.Fatal("Error creating the template")
	}
	defer f.Close()
	io.Copy(f, strings.NewReader(str))
}
